from equilibrator_api import ComponentContribution, Q_     # Pour la recherche des données de ΔfG°'
from collections import OrderedDict                        # Sous-classe de dictionnaire mémorisant l'ordre d'entrée des données
import argparse  
import debug   # Pour la gestion des print


# ------------------------------------------------------------------------------------------------------------------------------------------
# CRÉATION DE VARIABLES POUR FACILITER L'EXPLOITATION DES DONNÉES
# ------------------------------------------------------------------------------------------------------------------------------------------
cc = ComponentContribution()    
data = OrderedDict()      
parser = argparse.ArgumentParser()      

# ------------------------------------------------------------------------------------------------------------------------------------------
# DÉFINITION DES CONDITIONS PHYSIOLOGIQUES DU CYTOSOL AFIN DE CALCULER LE ΔfG°'
# ------------------------------------------------------------------------------------------------------------------------------------------
cc.p_h = Q_(7.4)
cc.p_mg = Q_(3.0)
cc.ionic_strength = Q_("0.25M")
cc.temperature = Q_("298.15 K")

# ------------------------------------------------------------------------------------------------------------------------------------------
# FONCTIONS
# ------------------------------------------------------------------------------------------------------------------------------------------

# FONCTION SI PAS D ID DE REACTION DANS DOCUMENT TXT :
# ------------------------------------------------------------------------------------------------------------------------------------------

# Conversion du document input en dictionnaire
# ------------------------------------------------------------------------------------------------------------------------------------------
def get_dico_correspondance_id_metabolite_id_kegg(document):
    ''' Conversion des données de correspondances entre les ID de métabolites et ID de kegg issues du document introduit, en dictionnaire:
    INPUT : document   (file)
    Format : ID_métabolite;ID_kegg
    M_CO2;C00011
    M_ETOH;C00469
    ...
    OUTPUT : dico_correspondance_ID (dict)
    Format : dico_correspondance_ID = {'ID_métabolite':'ID_kegg', ...}
    {'M_CO2':'C00011', 'M_ETOH':'C00469', ...}
    '''
    dico_correspondance_ID = {}
    for line in document:                                # boucle for afin d'organiser tous les métabolites donnés
        (ID_metabolite, ID_kegg) = line.split(";")        # séparation des ID au ;
        ID_kegg = ID_kegg.strip("\n")                     # suppression de \n (symbole de retour à la ligne)
        dico_correspondance_ID[ID_metabolite] = ID_kegg 
    return dico_correspondance_ID

# Acquisition des données de masse et de ΔfG°' et organisation des données dans un dictionnaire au format {'ID_kegg':'[masse, ΔfG_standard_prime]',...}
# ------------------------------------------------------------------------------------------------------------------------------------------
def get_dico_deltafgO_mass_from_equilibrator_with_id_kegg(correspondance):
    ''' Acquisition des données de masse et de ΔfG°' à partir des ID_kegg et des données de l'api equilibrator et organisation des données dans un dictionnaire:
    INPUT : correspondance   (dict)   dictionnaire contenant l'ID_métabolite suivi de l'ID kegg correspondante
    Format : correspondance = {'ID_métabolite':'ID_kegg', ...}
    {'M_CO2':'C00011', 'M_ETOH':'C00469', ...}
    OUTPUT : dico_donnees_metabolites_M_G  (dict)
    Format : dico_donnees_metabolites_M_G = {'ID_kegg':[masse, ΔfG°'], ...}
    {'C00011': [44.00899999999999, -386.0000000000019], 'C00469': [46.068999999999996, 79.02538175410186], ...}
    '''
    dico_donnees_metabolites_M_G = {}
    donnees_masse_ΔfG_standard_prime = []

    for ID_kegg in correspondance():             
        ID_metabolite = "kegg" + ":" + ID_kegg               # l'ID keg a besoin d'être sous la forme (exemple de l'ATP): kegg:C00002  pour être pris en compte 
        metabolite = cc.get_compound(ID_metabolite)          # définition du métabolite à partir de l'ID kegg
        masse = (metabolite.mass)                            # aquisition de la masse moléculaire en dalton
        ΔGf_standard = cc.standard_dg_formation(metabolite)  # acquisition du ΔGf_standard en KJ/mol
        ΔfG = metabolite.transform(cc.p_h, cc.ionic_strength, cc.temperature, cc.p_mg).m_as("kJ/mol")    #acquisition du delta_ΔfG à partir des données de conditions physiologiques du cytosol définies précédemment
        ΔGf_standard = ΔGf_standard[0]     
        if ΔGf_standard == None :                            # pour le cas du métabolite H+ C00080
            ΔfG_standard_prime = 0
        else:
            ΔfG_standard_prime = ΔfG + ΔGf_standard      # calcul de ΔfG°' à partir de ΔfG° et ΔfG
        donnees_masse_ΔfG_standard_prime = [masse, ΔfG_standard_prime]
        (ID_kegg_sec, donnees) = (ID_kegg, donnees_masse_ΔfG_standard_prime)
        dico_donnees_metabolites_M_G[ID_kegg_sec] = donnees  
    return dico_donnees_metabolites_M_G

# FONCTION SI ID DE REACTION DANS DOCUMENT TXT :
# ------------------------------------------------------------------------------------------------------------------------------------------

# Conversion du document input en liste
# ------------------------------------------------------------------------------------------------------------------------------------------
def get_liste_correspondance_id_metabolite_id_kegg(document):
    ''' Conversion des données de correspondances entre les ID de métabolites et ID de kegg issues du document introduit, en liste:
    INPUT : document   (file)
    Format : ID_métabolite;ID_kegg
    M_CO2;C00011
    M_ETOH;C00469
    ...
    OUTPUT : liste_correspondance_ID 
    Format : dico_correspondance_ID = [[ID_reac, ID_metabolite, ID_kegg],...]
    '''
    liste_correspondance_ID = []
    for line in document:                                # boucle for afin d'organiser tous les métabolites donnés
        (ID_reac, ID_metabolite, ID_kegg) = line.split(";")        # séparation des ID au ;
        ID_kegg = ID_kegg.strip("\n")                     # suppression de \n (symbole de retour à la ligne)
        liste_correspondance_ID.append([ID_reac, ID_metabolite, ID_kegg])
    return liste_correspondance_ID

# Acquisition des données de masse et de ΔfG°' et organisation des données dans une liste
# ------------------------------------------------------------------------------------------------------------------------------------------
def get_info_deltafgO_mass_from_equilibrator_with_id_kegg(correspondance):
    ''' Acquisition des données de masse et de ΔfG°' à partir des ID_kegg et des données de l'api equilibrator et organisation des données dans une liste:
    INPUT : liste_correspondance
    Format : liste_correspondance = [[ID_reac, ID_metabolite, ID_kegg],...]
    OUTPUT : donnees_metabolites_M_G  (dict)
    Format : dico_donnees_metabolites_M_G = [[ID_kegg[2], [masse, ΔfG_standard_prime]],...]
    '''
    donnees_metabolites_M_G = []
    donnees_masse_ΔfG_standard_prime = []

    for ID_kegg in correspondance:             
        ID_metabolite = "kegg" + ":" + ID_kegg[2]               # l'ID keg a besoin d'être sous la forme (exemple de l'ATP): kegg:C00002  pour être pris en compte 
        metabolite = cc.get_compound(ID_metabolite)          # définition du métabolite à partir de l'ID kegg
        masse = (metabolite.mass)                            # aquisition de la masse moléculaire en dalton
        try:
            ΔGf_standard = cc.standard_dg_formation(metabolite)  # acquisition du ΔGf_standard en KJ/mol
            ΔfG = metabolite.transform(cc.p_h, cc.ionic_strength, cc.temperature, cc.p_mg).m_as("kJ/mol")    #acquisition du delta_ΔfG à partir des données de conditions physiologiques du cytosol définies précédemment
            ΔGf_standard = ΔGf_standard[0]     
        except:
            ΔGf_standard = "None"
        if ΔGf_standard == None or ΔGf_standard == "None":                            # pour le cas du métabolite H+ C00080
            ΔfG_standard_prime = 0
        else:
            ΔfG_standard_prime = ΔfG + ΔGf_standard      # calcul de ΔfG°' à partir de ΔfG° et ΔfG
        donnees_masse_ΔfG_standard_prime = [masse, ΔfG_standard_prime]
        (ID_kegg_sec, donnees) = (ID_kegg[2], donnees_masse_ΔfG_standard_prime) # peut - être inutile
        donnees_metabolites_M_G.append([ID_kegg_sec,donnees])
    return donnees_metabolites_M_G


# Fonction Principale

def get_acquisition_metabolite(dico_correspondance_id_metabolite_id_kegg):
    '''Fonction principale permettant d'acquérir les données de masse et de ΔfG°' à partir des ID_kegg et des données de l'api equilibrator et de les organiser dans un dictionnaire:
    Input : dictionnaire de correspondance entre les ID de métabolites et les ID kegg
    Output : dictionnaire contenant les données de masse et de ΔfG°' organisées par ID kegg
    '''
    
    dictionnaire_deltafgO_mass_by_id_metabolite = get_dico_deltafgO_mass_from_equilibrator_with_id_kegg(dico_correspondance_id_metabolite_id_kegg.values)  

    return dictionnaire_deltafgO_mass_by_id_metabolite