## OBJECTIF: 
# L'objectif de ce programme est la récupération des données de l'énergie de réaction standard de Gibbs et de la constante de réaction à partir d'un document contenant le kegg des métabolites de réactions
# Les données seront récupérées à partir de l'API equilibrator.

from equilibrator_api import ComponentContribution, Q_   # Pour la recherche des données de ΔrG°'
from math import exp                                     # Pour utiliser la fonction exponentielle
#from pyexcel_ods3 import save_data                       # Pour la création et modification d'un fichier au format .ods
from collections import OrderedDict                      # Sous-classe de dictionnaire mémorisant l'ordre d'entrée des données
import argparse  
import requests
import debug

# ------------------------------------------------------------------------------------------------------------------------------------------
# CRÉATION DE VARIABLES POUR FACILITER L'EXPLOITATION DES DONNÉES
# ------------------------------------------------------------------------------------------------------------------------------------------

cc = ComponentContribution()
data = OrderedDict()
parser = argparse.ArgumentParser()

# ------------------------------------------------------------------------------------------------------------------------------------------
# FONCTIONS
# ------------------------------------------------------------------------------------------------------------------------------------------

def recup_equation_from_id_kegg_react(id_kegg):
    '''Récupération de l'équation d'une réaction à partir de son ID kegg
    INPUT : id_kegg (str)
    Format : id_kegg = 'RXXXXX'
    OUTPUT : equation (str)
    Format : equation = 'C00001 + C00002 = C00008 + C00009'
    '''
    url = f"https://rest.kegg.jp/get/reaction:{id_kegg}"
    r = requests.get(url)
    if r.status_code == 200:
        text = r.text
        lines = text.split("\n")
        equation = ""
        for line in lines:
            if "EQUATION" in line:
                equation = line
        return equation
    else:
        return None
    
def modification_equation(equation):
    '''Modification de l'équation d'une réaction pour qu'elle soit utilisable par l'API equilibrator
    INPUT : equation (str)
    Format : equation = 'EQUATION    C00001 + C00002 = C00008 + C00009'
    OUTPUT : equation (str)
    Format : equation = 'kegg:C00001 + kegg:C00002 = kegg:C00008 + kegg:C00009'
    '''
    # enlever "EQUATION    "
    equation = equation.replace("EQUATION    ", "")
    # remplacement des ID kegg par kegg:ID
    equation = equation.replace("C", "kegg:C")
    return equation

def get_dico_correspondance_id_reaction_equation(list_reaction):
    ''' A partir d'une liste de réaction donnée, récupération des équations de réactions correspondantes: formation d'un dictionnaire
    INPUT : list_reaction (list)
    Format : list_reaction = ['RXXXXX', 'RYYYYY', ...]
    OUTPUT : dico_correspondance_id_reaction_equation (dict)
    Format : dico_correspondance_id_reaction_equation = {'ID_réaction':'equation', ...}
    {'RXXXXX':'kegg:C00001 + kegg:C00002 = kegg:C00008 + kegg:C00009', 'RYYYYY':'kegg:C00001 + kegg:C00002 = kegg:C00008 + kegg:C00009', ...}
    '''
    dico_correspondance_id_reaction_equation = {}
    for id_reaction in list_reaction:
        equation = recup_equation_from_id_kegg_react(id_reaction)
        equation = modification_equation(equation)
        dico_correspondance_id_reaction_equation[id_reaction] = equation
    return dico_correspondance_id_reaction_equation


# Recherche et calcul du Delta r G°'  et Keq en fonction de l'equation (ses kegg)
# ------------------------------------------------------------------------------------------------------------------------------------------
def get_donnees(ID):
    '''Utilisation de l'API equilibrator afin de trouver delta r G°' puis de calculer le K eq à partir des équations avec kegg provenant d'un dictionnaire, les données sont retournées en deux listes:
    INPUT : correspondance   (dict)   dictionnaire contenant l'ID_réaction suivi de l'equation avec les ID kegg des métabolites 
    Format : correspondance = {'ID_réaction:'equation', ...}
    {'ATP_hydrolysis':'kegg:C00001 + kegg:C00002 = kegg:C00008 + kegg:C00009', ...}
    OUTPUT : faire des dictionnaires avec les données de delta_G et Keq suivant l'ID de la réaction
    dictionnaire_delta_G = {'ID_réaction':delta_G, ...}
    {'ATP_hydrolysis':-30.5, ...}
    dictionnaire_Keq = {'ID_réaction':Keq, ...}
    {'ATP_hydrolysis':1.5, ...}
    '''
    
    # Création d'une liste contenant les ID de toutes les équations dans l'ordre
    equation_liste = []
    id_react_liste = []
    for id_react, equation in ID.items():
        equation_ID = cc.parse_reaction_formula(equation)  # utilisation de l'api equilibrator afin de transformer l'equation en une ID permettant à la suite de trouver le delta r G°'
        equation_liste.append(equation_ID)
        id_react_liste.append(id_react)
    # Recherche des delta r G°' et des incertitudes
    (
    standard_dg_prime, dg_uncertainty
    ) = cc.standard_dg_prime_multi(equation_liste, uncertainty_representation="cov")  # Création d'un array contenant tous les delta r G°' suivi de 'Kilojoule / mole'
    dictionnaire_delta_G = {}
    dictionnaire_Keq = {}
    for i in range(len(standard_dg_prime)):
        element = float(str(standard_dg_prime[i]).strip(" kilojoule / mole"))     # suppression de 'Kilojoule/mole' afin de retransformer element en nombre
        dictionnaire_delta_G[id_react_liste[i]] = element
        dictionnaire_Keq[id_react_liste[i]] = exp((element)*(10**3)/(-8.314*298.15))
    return dictionnaire_delta_G, dictionnaire_Keq


def get_acquisition_reaction(liste_reactions):
    '''Fonction principale permettant d'acquérir les données de ΔrG°' et de Keq à partir des ID_kegg et des données de l'api equilibrator et de les organiser dans un dictionnaire:
    Input : dictionnaire de correspondance entre les ID de réactions et les équations de réactions
    Output : dictionnaire contenant les données de ΔrG°' et de Keq organisées par ID réaction
    '''
    
    # récupération des équations de réactions correspondantes : C00001 + C00002 = C00008 + C00009
    dico_correspondance_id_reaction_equation = get_dico_correspondance_id_reaction_equation(liste_reactions)  
    debug.print_auto(f"Récupération des équations de réactions correspondantes : {dico_correspondance_id_reaction_equation}", debug)
    
    # Recherdes caractéristiques des constantes de réaction
    delta_G, Keq_full = get_donnees(dico_correspondance_id_reaction_equation)
    

    return delta_G, Keq_full